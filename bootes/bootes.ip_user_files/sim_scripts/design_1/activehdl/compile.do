vlib work
vlib activehdl

vlib activehdl/xil_defaultlib
vlib activehdl/xpm
vlib activehdl/axi_lite_ipif_v3_0_4
vlib activehdl/lib_cdc_v1_0_2
vlib activehdl/interrupt_control_v3_1_4
vlib activehdl/axi_gpio_v2_0_15
vlib activehdl/proc_sys_reset_v5_0_11
vlib activehdl/generic_baseblocks_v2_1_0
vlib activehdl/axi_infrastructure_v1_1_0
vlib activehdl/axi_register_slice_v2_1_13
vlib activehdl/fifo_generator_v13_1_4
vlib activehdl/axi_data_fifo_v2_1_12
vlib activehdl/axi_crossbar_v2_1_14
vlib activehdl/axi_protocol_converter_v2_1_13
vlib activehdl/axi_clock_converter_v2_1_12
vlib activehdl/blk_mem_gen_v8_3_6
vlib activehdl/axi_dwidth_converter_v2_1_13

vmap xil_defaultlib activehdl/xil_defaultlib
vmap xpm activehdl/xpm
vmap axi_lite_ipif_v3_0_4 activehdl/axi_lite_ipif_v3_0_4
vmap lib_cdc_v1_0_2 activehdl/lib_cdc_v1_0_2
vmap interrupt_control_v3_1_4 activehdl/interrupt_control_v3_1_4
vmap axi_gpio_v2_0_15 activehdl/axi_gpio_v2_0_15
vmap proc_sys_reset_v5_0_11 activehdl/proc_sys_reset_v5_0_11
vmap generic_baseblocks_v2_1_0 activehdl/generic_baseblocks_v2_1_0
vmap axi_infrastructure_v1_1_0 activehdl/axi_infrastructure_v1_1_0
vmap axi_register_slice_v2_1_13 activehdl/axi_register_slice_v2_1_13
vmap fifo_generator_v13_1_4 activehdl/fifo_generator_v13_1_4
vmap axi_data_fifo_v2_1_12 activehdl/axi_data_fifo_v2_1_12
vmap axi_crossbar_v2_1_14 activehdl/axi_crossbar_v2_1_14
vmap axi_protocol_converter_v2_1_13 activehdl/axi_protocol_converter_v2_1_13
vmap axi_clock_converter_v2_1_12 activehdl/axi_clock_converter_v2_1_12
vmap blk_mem_gen_v8_3_6 activehdl/blk_mem_gen_v8_3_6
vmap axi_dwidth_converter_v2_1_13 activehdl/axi_dwidth_converter_v2_1_13

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"/opt/Xilinx/Vivado/2017.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/opt/Xilinx/Vivado/2017.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"/opt/Xilinx/Vivado/2017.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../bd/design_1/ip/design_1_zynq_ultra_ps_e_0_0/sim/design_1_zynq_ultra_ps_e_0_0.v" \

vcom -work axi_lite_ipif_v3_0_4 -93 \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/0ba0/hdl/axi_lite_ipif_v3_0_vh_rfs.vhd" \

vcom -work lib_cdc_v1_0_2 -93 \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/52cb/hdl/lib_cdc_v1_0_rfs.vhd" \

vcom -work interrupt_control_v3_1_4 -93 \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/e956/hdl/interrupt_control_v3_1_vh_rfs.vhd" \

vcom -work axi_gpio_v2_0_15 -93 \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/cb07/hdl/axi_gpio_v2_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_axi_gpio_0_0/sim/design_1_axi_gpio_0_0.vhd" \

vcom -work proc_sys_reset_v5_0_11 -93 \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/5db7/hdl/proc_sys_reset_v5_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_rst_ps8_0_99M_0/sim/design_1_rst_ps8_0_99M_0.vhd" \
"../../../bd/design_1/ip/design_1_axi_gpio_1_0/sim/design_1_axi_gpio_1_0.vhd" \
"../../../bd/design_1/ip/design_1_axi_gpio_2_0/sim/design_1_axi_gpio_2_0.vhd" \

vlog -work generic_baseblocks_v2_1_0  -v2k5 "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/f9c1/hdl/generic_baseblocks_v2_1_vl_rfs.v" \

vlog -work axi_infrastructure_v1_1_0  -v2k5 "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl/axi_infrastructure_v1_1_vl_rfs.v" \

vlog -work axi_register_slice_v2_1_13  -v2k5 "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/55c0/hdl/axi_register_slice_v2_1_vl_rfs.v" \

vlog -work fifo_generator_v13_1_4  -v2k5 "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/ebc2/simulation/fifo_generator_vlog_beh.v" \

vcom -work fifo_generator_v13_1_4 -93 \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/ebc2/hdl/fifo_generator_v13_1_rfs.vhd" \

vlog -work fifo_generator_v13_1_4  -v2k5 "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/ebc2/hdl/fifo_generator_v13_1_rfs.v" \

vlog -work axi_data_fifo_v2_1_12  -v2k5 "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/95b9/hdl/axi_data_fifo_v2_1_vl_rfs.v" \

vlog -work axi_crossbar_v2_1_14  -v2k5 "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/f582/hdl/axi_crossbar_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../bd/design_1/ip/design_1_xbar_0/sim/design_1_xbar_0.v" \
"../../../bd/design_1/hdl/design_1.v" \

vlog -work axi_protocol_converter_v2_1_13  -v2k5 "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/f0ae/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \

vlog -work axi_clock_converter_v2_1_12  -v2k5 "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/80fd/hdl/axi_clock_converter_v2_1_vl_rfs.v" \

vlog -work blk_mem_gen_v8_3_6  -v2k5 "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/4158/simulation/blk_mem_gen_v8_3.v" \

vlog -work axi_dwidth_converter_v2_1_13  -v2k5 "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/3bde/hdl/axi_dwidth_converter_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../bd/design_1/ip/design_1_auto_ds_0/sim/design_1_auto_ds_0.v" \
"../../../bd/design_1/ip/design_1_auto_pc_0/sim/design_1_auto_pc_0.v" \

vlog -work xil_defaultlib \
"glbl.v"

