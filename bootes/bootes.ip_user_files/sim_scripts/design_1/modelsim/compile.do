vlib work
vlib msim

vlib msim/xil_defaultlib
vlib msim/xpm
vlib msim/axi_lite_ipif_v3_0_4
vlib msim/lib_cdc_v1_0_2
vlib msim/interrupt_control_v3_1_4
vlib msim/axi_gpio_v2_0_15
vlib msim/proc_sys_reset_v5_0_11
vlib msim/generic_baseblocks_v2_1_0
vlib msim/axi_infrastructure_v1_1_0
vlib msim/axi_register_slice_v2_1_13
vlib msim/fifo_generator_v13_1_4
vlib msim/axi_data_fifo_v2_1_12
vlib msim/axi_crossbar_v2_1_14
vlib msim/axi_protocol_converter_v2_1_13
vlib msim/axi_clock_converter_v2_1_12
vlib msim/blk_mem_gen_v8_3_6
vlib msim/axi_dwidth_converter_v2_1_13

vmap xil_defaultlib msim/xil_defaultlib
vmap xpm msim/xpm
vmap axi_lite_ipif_v3_0_4 msim/axi_lite_ipif_v3_0_4
vmap lib_cdc_v1_0_2 msim/lib_cdc_v1_0_2
vmap interrupt_control_v3_1_4 msim/interrupt_control_v3_1_4
vmap axi_gpio_v2_0_15 msim/axi_gpio_v2_0_15
vmap proc_sys_reset_v5_0_11 msim/proc_sys_reset_v5_0_11
vmap generic_baseblocks_v2_1_0 msim/generic_baseblocks_v2_1_0
vmap axi_infrastructure_v1_1_0 msim/axi_infrastructure_v1_1_0
vmap axi_register_slice_v2_1_13 msim/axi_register_slice_v2_1_13
vmap fifo_generator_v13_1_4 msim/fifo_generator_v13_1_4
vmap axi_data_fifo_v2_1_12 msim/axi_data_fifo_v2_1_12
vmap axi_crossbar_v2_1_14 msim/axi_crossbar_v2_1_14
vmap axi_protocol_converter_v2_1_13 msim/axi_protocol_converter_v2_1_13
vmap axi_clock_converter_v2_1_12 msim/axi_clock_converter_v2_1_12
vmap blk_mem_gen_v8_3_6 msim/blk_mem_gen_v8_3_6
vmap axi_dwidth_converter_v2_1_13 msim/axi_dwidth_converter_v2_1_13

vlog -work xil_defaultlib -64 -incr -sv "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"/opt/Xilinx/Vivado/2017.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/opt/Xilinx/Vivado/2017.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -64 -93 \
"/opt/Xilinx/Vivado/2017.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../bd/design_1/ip/design_1_zynq_ultra_ps_e_0_0/sim/design_1_zynq_ultra_ps_e_0_0.v" \

vcom -work axi_lite_ipif_v3_0_4 -64 -93 \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/0ba0/hdl/axi_lite_ipif_v3_0_vh_rfs.vhd" \

vcom -work lib_cdc_v1_0_2 -64 -93 \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/52cb/hdl/lib_cdc_v1_0_rfs.vhd" \

vcom -work interrupt_control_v3_1_4 -64 -93 \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/e956/hdl/interrupt_control_v3_1_vh_rfs.vhd" \

vcom -work axi_gpio_v2_0_15 -64 -93 \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/cb07/hdl/axi_gpio_v2_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_1/ip/design_1_axi_gpio_0_0/sim/design_1_axi_gpio_0_0.vhd" \

vcom -work proc_sys_reset_v5_0_11 -64 -93 \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/5db7/hdl/proc_sys_reset_v5_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_1/ip/design_1_rst_ps8_0_99M_0/sim/design_1_rst_ps8_0_99M_0.vhd" \
"../../../bd/design_1/ip/design_1_axi_gpio_1_0/sim/design_1_axi_gpio_1_0.vhd" \
"../../../bd/design_1/ip/design_1_axi_gpio_2_0/sim/design_1_axi_gpio_2_0.vhd" \

vlog -work generic_baseblocks_v2_1_0 -64 -incr "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/f9c1/hdl/generic_baseblocks_v2_1_vl_rfs.v" \

vlog -work axi_infrastructure_v1_1_0 -64 -incr "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl/axi_infrastructure_v1_1_vl_rfs.v" \

vlog -work axi_register_slice_v2_1_13 -64 -incr "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/55c0/hdl/axi_register_slice_v2_1_vl_rfs.v" \

vlog -work fifo_generator_v13_1_4 -64 -incr "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/ebc2/simulation/fifo_generator_vlog_beh.v" \

vcom -work fifo_generator_v13_1_4 -64 -93 \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/ebc2/hdl/fifo_generator_v13_1_rfs.vhd" \

vlog -work fifo_generator_v13_1_4 -64 -incr "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/ebc2/hdl/fifo_generator_v13_1_rfs.v" \

vlog -work axi_data_fifo_v2_1_12 -64 -incr "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/95b9/hdl/axi_data_fifo_v2_1_vl_rfs.v" \

vlog -work axi_crossbar_v2_1_14 -64 -incr "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/f582/hdl/axi_crossbar_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../bd/design_1/ip/design_1_xbar_0/sim/design_1_xbar_0.v" \
"../../../bd/design_1/hdl/design_1.v" \

vlog -work axi_protocol_converter_v2_1_13 -64 -incr "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/f0ae/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \

vlog -work axi_clock_converter_v2_1_12 -64 -incr "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/80fd/hdl/axi_clock_converter_v2_1_vl_rfs.v" \

vlog -work blk_mem_gen_v8_3_6 -64 -incr "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/4158/simulation/blk_mem_gen_v8_3.v" \

vlog -work axi_dwidth_converter_v2_1_13 -64 -incr "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../../bootes.srcs/sources_1/bd/design_1/ipshared/3bde/hdl/axi_dwidth_converter_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" "+incdir+../../../../bootes.srcs/sources_1/bd/design_1/ipshared/7e3a/hdl" \
"../../../bd/design_1/ip/design_1_auto_ds_0/sim/design_1_auto_ds_0.v" \
"../../../bd/design_1/ip/design_1_auto_pc_0/sim/design_1_auto_pc_0.v" \

vlog -work xil_defaultlib \
"glbl.v"

